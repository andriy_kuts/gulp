'use strict';

var gulp = require('gulp'),
	autoprefixer = require('gulp-autoprefixer'),
	imagemin = require('gulp-imagemin'),
	pngquant = require('imagemin-pngquant'),
	svgo = require('imagemin-svgo'),
 	uglify = require('gulp-uglify'),
	cssmin = require('gulp-cssmin'),
	rename = require('gulp-rename'),
	sass = require('gulp-sass'),
	sourcemaps = require('gulp-sourcemaps'),
	plumber = require('gulp-plumber'),
	runSequence = require('run-sequence'),
	notify = require("gulp-notify"),
	gulpif = require("gulp-if"),
	changed = require('gulp-changed'),
	concat = require('gulp-concat'),
	fileinclude = require('gulp-file-include'),
	del = require('del'),
 	browserSync = require('browser-sync'),
	inject = require('gulp-inject');
	
	
var	reload = browserSync.reload;
var plumberErrorNotify = {
	errorHandler: notify.onError('Error: <%= error.message %>')
};


var destFolder, 
	serve = false;

// clean 
// del all except for minifyed images 
gulp.task('clean', function() {
	return del([ destFolder ]);
});

// images
gulp.task('images', function() {
	return gulp.src(['src/img/**/*'])
		.pipe(plumber(plumberErrorNotify))
		.pipe(changed(destFolder+'/img'))
		.pipe(
			gulpif(!serve, 
				imagemin({
					optimizationLevel: 3,
					progressive: true
				}, svgo([
				{
				  removeViewBox: false,
				  removeUselessStrokeAndFill: true,
				  removeEmptyAttrs: true
				}
				]), {
				use: [pngquant()]
				})
			)	
		)
		.pipe(gulp.dest(destFolder+'/img'))
		.pipe( gulpif(serve, reload({stream: true})) )
});

// data
gulp.task('data', function() {
	return gulp.src(['src/data/**/*'])
		.pipe(plumber(plumberErrorNotify))
		.pipe(changed(destFolder+'/data'))
		.pipe(gulp.dest(destFolder+'/data'))
		.pipe( gulpif(serve, reload({stream: true})) );
});

// fonts 
gulp.task('fonts', function() {
	return gulp.src(['src/fonts/**/*'])
		.pipe(plumber(plumberErrorNotify))
		.pipe(changed(destFolder+'/fonts'))
		.pipe(gulp.dest(destFolder+'/fonts'))
		.pipe( gulpif(serve, reload({stream: true})) );
});

// styles  serve
gulp.task('styles-serve', function() {
	 return gulp.src('src/styles/main.scss')
		.pipe(plumber(plumberErrorNotify))
  		.pipe(sourcemaps.init())
		.pipe(sass())
		.pipe(autoprefixer({
			browsers: ['last 20 version']
		}))
  		.pipe(sourcemaps.write('./maps'))
		.pipe(gulp.dest( 'dev/css' ))
		.pipe( reload({stream: true}) );
});

// styles dist
gulp.task('styles', function() {
	return gulp.src(['src/styles/main.scss'])
		.pipe(plumber(plumberErrorNotify))
		.pipe(sass())
		.pipe(autoprefixer({
			browsers: ['last 20 version']
		}))
		.pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest( 'dist/css' ))
});


// scripts  
gulp.task('scripts', function() {
	return gulp.src(['src/scripts/**/!(main)*.js', 'src/scripts/main.js'])
		.pipe(plumber(plumberErrorNotify))
		.pipe(sourcemaps.init())
		.pipe(concat('main.js'))
		.pipe(gulpif(serve , 
			uglify({
		     compress : false
	      	}), 
			uglify()
		)) 
		.pipe(gulpif(serve , sourcemaps.write('./maps')))
		.pipe(gulpif(!serve , rename({suffix : ".min"})))
		.pipe(gulp.dest(destFolder+'/js/'))
		.pipe(gulpif(serve , reload({stream: true}) ));
});

// html 
gulp.task('html',  function() {
	return gulp.src(['src/*.html'])
		.pipe(plumber(plumberErrorNotify))
		.pipe(gulpif(serve , changed('dev/*.html')) )
		.pipe(fileinclude({
			prefix: '@@',
			basepath: '@file'
		}))
		.pipe( 
			inject(
				gulp.src(
					[destFolder+'/js/*.js', destFolder+'/css/*.css'], 
					{read: false}
				), {
			      		transform: function (filepath) {
			      			filepath = filepath.replace('/'+destFolder+'/', '');

			         		if (filepath.slice(-3) === '.js') {
					          	return '<script src="'+filepath+'"></script>';
					        }
					        if (filepath.slice(-4) === '.css') {
					          	return '<link rel="stylesheet" href="'+filepath+'">';
					        }
			   			}
			    	}
      		) 
		)
		.pipe(gulp.dest(destFolder))
		.pipe( gulpif(serve, reload({stream: true})) );
});


// WATCH 
gulp.task('watch', function() {
	browserSync({
		tunnel: false,
		//tunnel: "gulpprojectstarter",
		https: false,
		notify: false,
		port: 8080,
		server: {
			baseDir: ['dev']
		}
	});

	gulp.watch(['src/*.html', 'src/inc/*.html'], ['html']);
	gulp.watch(['src/styles/**/*.scss'], ['styles-serve']);
	gulp.watch(['src/img/**/*'], ['images']);
	gulp.watch(['src/scripts/**/*.js'], ['scripts']);
	gulp.watch(['src/fonts/**/*'], ['fonts']);
 	gulp.watch(['src/data/**/*'], ['data']);
});

// SERVE  
gulp.task('serve', function() {
	serve =  true;
	destFolder = 'dev';

	runSequence(
		'clean',
		[
			'scripts',
			'styles-serve'
		],
		'html',
		'fonts',
		'images',
		'data',
		'watch'
	)
});


// DIST  
gulp.task('default', function() {
	serve =  false;
	destFolder = 'dist';

	runSequence(
	 	'clean',
		[
			'scripts',
			'styles'
		],
		'fonts',
		'data',
		'html',
		'images'
	)
});






 